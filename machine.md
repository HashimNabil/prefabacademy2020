# Machine Assignment
![](machine\machine.png)

## Y axis  

![](machine\y axis 1.jpeg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/nkIDtofHRTs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## X and Y axis
### Threading Y axis frame
![](machine\threadu.jpeg)

### Connecting Y axis frame to X axis Carriage
![](machine\connectYtoX.jpeg)

### Assembled X and Y frame

![](machine\fram1.jpeg)

### Manually moving the X and Y

<iframe width="560" height="315" src="https://www.youtube.com/embed/xB0ZDWoZ1lw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Lifting Mechanism

### Manually testing the lifting Mechanism

<iframe width="560" height="315" src="https://www.youtube.com/embed/s_11oAoWZw4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Installing the timing Belt

![](machine\timebelt1.jpeg)  

![](machine\pulley.jpeg)

![](machine\timebelt2.jpeg)  
